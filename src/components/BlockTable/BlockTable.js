import React, { Component } from 'react'
import moment from 'moment'
import { withRouter } from 'react-router-dom'

import './BlockTable.scss'

class BlockTable extends Component {
	render() {
		if (!this.props.data || !this.props.data.block)
			return <div></div>

		const blockList = this.props.data.block;

		return (
			<div className="block-table">
				{
					blockList.map((e, i) => (
						<div className="block-table__block" key={i} onClick={() => this.props.history.push(`${process.env.PUBLIC_URL}/blocks/${e.header.height}`)}>
							<div className="block-table__block__height">{ e.header.height }</div>
							<div className="block-table__block__time">{moment(e.header.timestamp.data * 1000).utc().format('MMMM Do YYYY, HH:mm:ss')}</div>
							<div className="block-table__block__hash mono" style={{width: 375}}>{ e.header.id.data }</div>
							<div className="block-table__block__txs" style={{width: 94, textAlign: 'right'}}><b>{ e.transactions ? e.transactions.length : 0 }</b> <span className="block-table__block__txs__text">transactions</span></div>
						</div>
					))
				}
			</div>
		)
	}
}

export default withRouter(BlockTable);
