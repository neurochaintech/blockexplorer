import React, { Component } from 'react'
import { connect } from 'react-redux'

import './StatusIndicator.scss'

class StatusIndicator extends Component {
	render() {
		if (this.props.isReady)
			return <div></div>
			
		return (
			<div className="status-indicator">
				<h1>
					Website under maintenance
				</h1>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		isReady: state.status.isReady
	}
}

export default connect(mapStateToProps, null)(StatusIndicator);
