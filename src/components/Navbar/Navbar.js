import React, { Component } from 'react'
import { NavLink, Link } from "react-router-dom"
import { connect } from 'react-redux'

import './Navbar.scss'

import Neurochain from '../../assets/neurochain.png'

class Navbar extends Component {
  render() {
    return (
      <div>
        <nav className="navbar">
          <div className="navbar__brand">
            <div className="navbar__brand__logo">
              <Link to={`${process.env.PUBLIC_URL}/`}>
                <img className="navbar__brand__logo__img" src={Neurochain} alt="" />
              </Link>
            </div>
          </div>
          <div className="navbar__menu">
            <div className="navbar__menu__right">
              <NavLink exact className="navbar__menu__right__link" to={`${process.env.PUBLIC_URL}/`}>Home</NavLink>
              {/*<NavLink className="navbar__menu__right__link" to={`${process.env.PUBLIC_URL}/blocks`}>Blocks</NavLink>*/}
              {/*<NavLink className="navbar__menu__right__link" to={`${process.env.PUBLIC_URL}/txs`}>Transactions</NavLink>*/}
            </div>
          </div>
        </nav>
      </div>
    )
  }
}

export default connect(null, null)(Navbar);
