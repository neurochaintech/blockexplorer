import axios from 'axios';

axios.defaults.baseURL = 'https://api.testnet.neurochaintech.io';
axios.defaults.timeout = 120000;

// axios.interceptors.response.use(function (response) {
//   return response;
// }, function (error) {
//   if (!error.response)
//   return Promise.reject(error);
// });

export default axios;
