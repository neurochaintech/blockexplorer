import React, { Component } from 'react';
import { Route } from "react-router-dom";
import { connect } from 'react-redux'

import './App.scss';

import Navbar from '../components/Navbar/Navbar'
import Home from './Home/Home'
import DetailsBlock from './DetailsBlock/DetailsBlock'
import DetailsTransaction from './DetailsTransaction/DetailsTransaction'
import StatusIndicator from '../components/StatusIndicator/StatusIndicator'
import DetailsAddress from './DetailsAddress/DetailsAddress';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App__container">
          <header>
            <Navbar />
          </header>
          <section className="App__container__content">
            <Route exact path={`${process.env.PUBLIC_URL}/`} component={Home} />
            <Route exact path={`${process.env.PUBLIC_URL}/blocks`} component={null} />
            <Route exact path={`${process.env.PUBLIC_URL}/txs`} component={null} />
            <Route exact path={`${process.env.PUBLIC_URL}/blocks/:val`} component={DetailsBlock} />
            <Route exact path={`${process.env.PUBLIC_URL}/txs/:val`} component={DetailsTransaction} />
            <Route exact path={`${process.env.PUBLIC_URL}/addr/:val`} component={DetailsAddress} />
          </section>
        </div>
        <StatusIndicator />
      </div>
    );
  }
}

export default connect(null, null)(App);
