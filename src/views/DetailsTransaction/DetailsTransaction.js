import React, { Component } from 'react'
import BlockService from '../../store/block/block.service'
// import Transaction from '../../mocks/transaction'

import './DetailsTransaction.scss'

import ReactJson from 'react-json-view'

class DetailsTransactions extends Component {
	state = {
		transactionId: decodeURIComponent(this.props.match.params.val),
		transaction: {}
	}

	componentWillMount() {
		BlockService.getTransactionFromId(this.state.transactionId)
			.then(r => {
				this.setState({
					transaction: r.data
				})
			})
	}

	render() {
		return (
			<div className="details-transaction">
				<div className="details-transaction__title">
					Transaction	<b>{this.state.transactionId}</b>
				</div>

				<div className="details-transaction__body">
					<div className="details-transaction__body__code">
						<ReactJson src={this.state.transaction} />
					</div>
				</div>
			</div>
		)
	}
}

export default DetailsTransactions
