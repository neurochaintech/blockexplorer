import React, { Component } from 'react'
import BlockService from '../../store/block/block.service'
// import Block from '../../mocks/block'

import './DetailsBlock.scss'

import ReactJson from 'react-json-view'

class DetailsBlock extends Component {
	state = {
		blockNumber: decodeURIComponent(this.props.match.params.val),
		block: {}
	}

	componentWillMount() {
		(this.state.blockNumber.length > 32 ? BlockService.getBlockFromId(this.state.blockNumber) : BlockService.getBlockFromHeight(this.state.blockNumber))
			.then(r => {
				this.setState({
					block: r.data
				})
			})
	}

	render() {
		return (
			<div className="details-block">
				<div className="details-block__title">
					Block	<b>{this.state.blockNumber}</b>
				</div>

				<div className="details-block__body">
					<div className="details-block__body__code">
						<ReactJson src={this.state.block} />
					</div>
				</div>
			</div>
		)
	}
}

export default DetailsBlock
