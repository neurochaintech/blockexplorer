import React, { Component } from 'react'

import './DetailsAddress.scss'

import Balance from '../../mocks/balance'
import ListTransactions from '../../mocks/list_transactions'

import ReactJson from 'react-json-view'

class DetailsAddress extends Component {
	state = {
		address: decodeURIComponent(this.props.match.params.val)
	}

	// add polling here whenever the service will be ready
	// componentDidMount() {
	// 	this.fetchData();

	// 	this.polling = setInterval(() => {
	// 		this.fetchData();
	// 	}, 1000 * (process.env.POLLING_TIME || 10)) // 10 sec default
	// }

	// fetchData() {

	// }

	render() {
		return (
			<div className="details-address">
				<div className="details-address__title">
					<div>Address	<b>{this.state.address}</b></div>
					<div className="details-address__title__balance"><b>{Balance.value}</b> NCC</div>
				</div>

				<div className="details-address__body">
					<div className="details-address__body__code">
						<ReactJson src={ListTransactions} />
					</div>
				</div>
			</div>
		)
	}
}

export default DetailsAddress
