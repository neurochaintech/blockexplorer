import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getNbBlocks, getNbTransactions } from '../../store/info/info.actions'
import { getLastBlocks } from '../../store/block/block.actions'
import { withRouter } from 'react-router-dom'

import './Home.scss'

import BlockTable from '../../components/BlockTable/BlockTable'

import Search from '../../assets/search.svg'

const searchTab = [
	{
		name: 'Blocks'
	},
	{
		name: 'Transactions'
	},
	{
		name: 'Address'
	}
]
class Home extends Component {
	state = {
		price: 'N/A',
		totalSupply: 'N/A',
		pii: 'N/A',
		lastBlocks: [],
		search: '',
		searchSelected: 0
	}

	constructor(props) {
		super(props);

		this.handleInput = this.handleInput.bind(this);
		this.handleEnter = this.handleEnter.bind(this);
	}

	componentDidMount() {
		window.addEventListener('keypress', this.handleEnter);

		this.startPolling();
	}

	async startPolling() {
		await this.fetchData();
		this.polling();
	}

	polling() {
		this.timeOut = setTimeout(async () => {
			await this.fetchData();
			this.polling();
		}, 1000 * (process.env.POLLING_TIME || 10)) // 10 sec default
	}

	fetchData() {
		return Promise.all([
			this.props.getNbBlocks(),
			this.props.getNbTransactions(),
			this.props.getLastBlocks(10)
		]);
	}

	componentWillUnmount() {
		window.removeEventListener('keypress', this.handleEnter);
		clearTimeout(this.timeOut);
	}

	handleEnter(e) {
		if (e.keyCode !== 13)
			return;

		if (this.state.search.length <= 0)
			return;

		const { searchSelected } = this.state;
		let type;

		switch (searchSelected) {
			case 0:
				type = 'blocks';
				break;
			case 1:
				type = 'txs';
				break;
			case 2:
				type = 'addr';
				break;
			default:
				type = 'blocks';
		}

		this.props.history.push(`${process.env.PUBLIC_URL}/${type}/${encodeURIComponent(this.state.search)}`);
	}

	handleInput(event) {
		this.setState({
			[event.target.name]: event.target.value
		});
	}

	render() {
		return (
			<div className="home">
				<div className="home__header">
					<div className="home__header__content">
						<div className="home__header__content__title">Blocks</div>
						<div className="home__header__content__value">{this.props.nbBlocks}</div>
					</div>
					<div className="home__header__content">
						<div className="home__header__content__title">Tx's</div>
						<div className="home__header__content__value">{this.props.nbTransactions}</div>
					</div>
					<div className="home__header__content">
						<div className="home__header__content__title">Pii</div>
						<div className="home__header__content__value">{this.state.pii}</div>
					</div>
					<div className="home__header__content">
						<div className="home__header__content__title">Price</div>
						<div className="home__header__content__value">{this.state.price}</div>
					</div>
					<div className="home__header__content">
						<div className="home__header__content__title">Total Supply</div>
						<div className="home__header__content__value">{this.state.totalSupply}</div>
					</div>
				</div>
				<div className="home__body">
					<div className="home__body__search">
						<div className="home__body__search__title">
							{
								searchTab.map((e, i) => (
									<div
										className={`home__body__search__title__tab ${i === this.state.searchSelected ? 'active' : ''}`}
										key={i}
										onClick={() => this.setState({ searchSelected: i })}>
										{e.name}
									</div>
								))
							}
						</div>
						<div className="home__body__search__group">
							<input type="text" name="search" onChange={this.handleInput} />
							<img className="home__body__search__group__icon" src={Search} alt="" />
						</div>
					</div>
				</div>
				<div className="home__latest-blocks">
					<BlockTable data={this.props.lastBlocks} />
				</div>
			</div>
		)
	}
}

const mapDispatchToProps = {
	getNbBlocks,
	getNbTransactions,
	getLastBlocks
}

const mapStateToProps = state => {
	return {
		nbBlocks: state.info.nbBlocks,
		nbTransactions: state.info.nbTransactions,
		lastBlocks: state.block.lastBlocks
	}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Home));
