import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from "react-router-dom";

import './index.scss';

import App from './views/App';

import * as serviceWorker from './serviceWorker';

import { Provider } from 'react-redux'
import store from './store/store'
import { setReady } from './store/status/status.actions';

store.dispatch(setReady());

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>
, document.getElementById('root'));

serviceWorker.unregister();
