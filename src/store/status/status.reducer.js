const initialState = {
	isReady: true
}

const info = (state = initialState, action) => {
    const payload = action.payload;

    switch (action.type) {
      case 'SET_READY':
        return {
          ...state,
					isReady: payload
				}
      default:
        return state;
    }
  }

  export default info;
