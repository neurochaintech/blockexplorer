import api from '../../utils/api';

export default {
	isReady() {
		return api.get('/ready');
	}
};
