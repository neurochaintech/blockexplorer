import Service from './status.service';

export const setReady = () => {
	return async dispatch => {
		Service.isReady()
			.then(() => {
				dispatch({
					type: 'SET_READY',
					payload: true
				});
			})
			.catch(() => {
				dispatch({
					type: 'SET_READY',
					payload: false
				});
			});
	};
};
