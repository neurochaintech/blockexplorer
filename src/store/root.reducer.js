import { combineReducers } from 'redux'
import info from './info/info.reducer'
import block from './block/block.reducer'
import status from './status/status.reducer'

export default combineReducers({
	info,
	block,
	status
})
