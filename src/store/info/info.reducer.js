const initialState = {
	nbBlocks: 'N/A',
	nbTransactions: 'N/A'
}

const info = (state = initialState, action) => {
    const payload = action.payload;

    switch (action.type) {
      case 'SET_NB_BLOCKS':
        return {
          ...state,
					nbBlocks: payload
				}
			case 'SET_NB_TRANSACTIONS':
        return {
          ...state,
					nbTransactions: payload
        }
      default:
        return state;
    }
  }
  
  export default info;
