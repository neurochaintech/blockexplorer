import Service from './infos.service'

// mocks
// import NbBlocks from '../../mocks/total_nb_blocks'
// import NbTransactions from '../../mocks/total_nb_transactions'

export const getNbBlocks = () => {
	return async dispatch => {
		const result = await Service.getNbBlocks();
		const nbBlocks = result.data;

		// const nbBlocks = NbBlocks;

		dispatch({
			type: 'SET_NB_BLOCKS',
			payload: nbBlocks
		});
	}
}

export const getNbTransactions = () => {
	return async dispatch => {
		const result = await Service.getNbTransactions();
		const nbTransactions = result.data;

		// const nbTransactions = NbTransactions

		dispatch({
			type: 'SET_NB_TRANSACTIONS',
			payload: nbTransactions
		});
	}
}
