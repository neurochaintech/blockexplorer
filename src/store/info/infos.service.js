import api from '../../utils/api';

class Service {
	async getNbBlocks() {
		return await api.get('/total_nb_blocks');
	}

	async getNbTransactions() {
		return await api.get('/total_nb_transactions');
	}
}

let instance;

const getInstance = function () {
	if (!instance) {
		instance = new Service();
	}
	return instance;
};


export default getInstance();
