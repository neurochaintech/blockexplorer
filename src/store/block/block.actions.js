import Service from './block.service'
// mock
// import LastBlocks from '../../mocks/last_blocks'

export const getLastBlocks = (amount) => {
	return async dispatch => {
		const result = await Service.getLastBlocks(amount);
		const lastBlocks = result.data;

		// const lastBlocks = LastBlocks;

		dispatch({
			type: 'SET_LAST_BLOCKS',
			payload: lastBlocks
		})
	};
}
