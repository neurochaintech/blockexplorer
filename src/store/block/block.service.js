import api from '../../utils/api';

class Service {
	async getLastBlocks(nb) {
		return await api.get(`/last_blocks/${nb}`);
	}

	async getBlockFromHeight(height) {
		return await api.get(`/block/height/${height}`);
	}

	async getBlockFromId(id) {
		return await api.post('/block/id', { data: id });
	}

	async getTransactionFromId(id) {
		return await api.post('/transaction', { data: id });
	}
}

let instance;

const getInstance = function () {
	if (!instance) {
		instance = new Service();
	}
	return instance;
};


export default getInstance();
