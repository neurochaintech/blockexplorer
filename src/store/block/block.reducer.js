const initialState = {
	lastBlocks: []
}

const info = (state = initialState, action) => {
    const payload = action.payload;

    switch (action.type) {
      case 'SET_LAST_BLOCKS':
        return {
          ...state,
					lastBlocks: payload
				}
      default:
        return state;
    }
  }
  
  export default info;
