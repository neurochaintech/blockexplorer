# Need 

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Need](#need)
    - [Last Block](#last-block)
    - [Display Blocks](#display-blocks)
    - [Find Transactions](#find-transactions)
- [How](#how)
- [Misc](#misc)

<!-- markdown-toc end -->


## Last Block 

Display ID and height for last known block.

## Display Blocks

Inputs:
* Height.
* ID/hash.

Output:
* [BlockHeader structure](https://gitlab.com/neurochaintech/core/blob/feature/ledger/src/messages/messages.proto).
* Coinbase value.
* Number of transactions.

## Find Transactions

Inputs: 
* Transaction ID.
* Input address.
* Output address.
* Any inputs for blocks (see above).

Output:
* [Transaction structure](https://gitlab.com/neurochaintech/core/blob/feature/ledger/src/messages/messages.proto).
* Block ID and height.


# How 

* Static web pages (could typically hosted in S3).
* Requesting bot over http api (almost REST).

# Misc

First version does not need to manage branches. 
